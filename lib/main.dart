import 'dart:ui';
import 'package:flutter/material.dart';
import 'dart:math';

void main() => runApp(MyApp());

///Global Variables for Nutritional Diary
int counter = 0;
String food1 = "";
String food2 = "";
String food3 = "";
String food4 = "";
String food5 = "";

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab 2',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PageController _pageController = PageController();
  List<Widget> _screens = [ Home(), Diary(), Workouts() ];
  int _selectedIndex = 0;

  void _onPageChanged(int index) { setState(() {_selectedIndex = index;});}

  void _onitemTapped(int selectedIndex) {
    _pageController.jumpToPage(selectedIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        children: _screens,
        onPageChanged: _onPageChanged,
        physics: NeverScrollableScrollPhysics(),

      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.red,
        onTap: _onitemTapped,
        items: [
        BottomNavigationBarItem(
            icon: Icon(Icons.home, color: _selectedIndex == 0 ? Colors.white : Colors.red[200],),
            title: Text('Home', style: TextStyle(color: _selectedIndex == 0 ? Colors.white : Colors.red[200])),
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.book, color: _selectedIndex == 1 ? Colors.white : Colors.red[200]),
            title: Text('Nutritional Diary', style: TextStyle(color: _selectedIndex == 1 ? Colors.white : Colors.red[200])),
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.directions_walk, color: _selectedIndex == 2 ? Colors.white : Colors.red[200],),
            title: Text('Workouts', style: TextStyle(color: _selectedIndex == 2 ? Colors.white : Colors.red[200],)),
        ),

        ],
      ),
    );
  }
}

///Home Page
///-----------------------------------------------------------------------------
class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  String randomizeQuotes() {
    Random rnd;
    int min = 1; int max = 8;
    rnd = new Random();
    int r = min + rnd.nextInt(max - min);

    String quote;
    switch(r) {
      case 1: {quote = '“You miss 100% of the shots you don’t take”';}
      break;

      case 2: {quote = '“The difference between the impossible and the possible lies in a person’s determination”';}
      break;

      case 3: {quote = '"Of course it’s hard. It’s supposed to be hard. If it were easy, everybody would do it. Hard is what makes it great"';}
      break;

      case 4: {quote = '“The pain you feel today, will be the strength you feel tomorrow"';}
      break;

      case 5: {quote = '"Never give up on a dream just because of the time it will take to accomplish it. The time will pass anyway"';}
      break;

      case 6: {quote = '"The only person you are destined to become is the person you decide to be."';}
      break;

      case 7: {quote = '“The last three or four reps is what makes the muscle grow. This area of pain divides a champion from someone who is not a champion”';}
      break;

      default: {quote = '“Don’t limit your challenges, challenge your limits”';}
      break;
    }
    return quote;
  }

  void _showBottomSheetCallback(){
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return Container(
        decoration: BoxDecoration(
          border: Border(top: BorderSide(color: Colors.black)),
          color: Colors.red[100],
        ),
        child: Padding(
          padding: const EdgeInsets.all(32),
          child: Text(randomizeQuotes(), textAlign: TextAlign.center, style: TextStyle(fontSize: 24.0),
        ),
      )
      );
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Home'),
        backgroundColor: Colors.red,
      ),
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("images/group.jpeg", ),
                fit: BoxFit.fill,
              ),
            ),
          ),

          Center(/*
            child: RaisedButton(
            onPressed: _showBottomSheetCallback,
            child: const Text('Show todays quote'),
              color: Colors.white,
            ),*/
            child:
              RawMaterialButton(
                onPressed: _showBottomSheetCallback,
                elevation: 2.0,
                fillColor: Colors.white,
                child: Icon(
                  Icons.format_quote,
                  size: 35.0,
                ),
                padding: EdgeInsets.all(15.0),
                shape: CircleBorder(),
              )

          ),
        ]
      ),
    );
  }
}

///Nutritional Diary Page
///-----------------------------------------------------------------------------
class Diary extends StatefulWidget {
  @override
  _DiaryState createState() => _DiaryState();
}

class _DiaryState extends State<Diary> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void storeMealInfo(String str) {
    if(counter == 0)
      food1 = str;

    if(counter == 1)
      food2 = str;

    if(counter == 2)
      food3 = str;

    if(counter == 3)
      food4 = str;

    if(counter == 4)
      food5 = str;

    counter++;
  }

  void removeMeal() {
    if(counter != 0)
      counter--;

    if(counter == 0)
      food1 = '';

    if(counter == 1)
      food2 = '';

    if(counter == 2)
      food3 = '';

    if(counter == 3)
      food4 = '';

    if(counter == 4)
      food5 = '';
  }

  String gatherMeals() {
    String finalfood = '';

    if(counter != 0)
      finalfood = food1 + '                                                  ' + '\n' + food2 + '                                                  ' + '\n' + food3 + '                                                  ' + '\n' + food4 + '                                                  ' + '\n' + food5 + '                                                  ';

    else if(counter == 0)
      finalfood = '     No meals entered yet today          ';

    print('$counter');

    return finalfood;
  }

  ///Enter meal
  createAlertDialog(BuildContext context) {
    TextEditingController controller = TextEditingController();
    return showDialog(context: context, builder: (context) {
      return AlertDialog(
        title: Text('Enter your meal '),
        content: TextField(
          controller: controller,
        ),
        actions: <Widget>[
          MaterialButton(
            elevation: 5.0,
            child: Text('Enter into Diary'),
            onPressed: () {
              storeMealInfo(controller.text.toString());
              Navigator.of(context).pop();
            },
          )
        ],
      );
    },);
  }

  void displayModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.add),
                    title: new Text('Add meal'),
                    onTap: () => {createAlertDialog(context)}),
                new ListTile(
                  leading: new Icon(Icons.remove),
                  title: new Text('Remove meal'),
                  onTap: () => {removeMeal()},
                ),
              ],
            ),
          );
        });
  }

  void _showBottomSheetCallback(){
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return Container(
          decoration: BoxDecoration(
              border: Border(top: BorderSide(color: Colors.black)),
              color: Colors.red[100],
          ),
          child: Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(20, 20, 20, 20),
            child: Text(gatherMeals(), textAlign: TextAlign.center, style: TextStyle(fontSize: 24.0),
            ),
          )
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Nutritional Diary'),
        backgroundColor: Colors.red,
      ),

      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("images/food.jpg",),
                fit: BoxFit.fill,
              ),
            ),
          ),

          Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                    onPressed: () {
                      displayModalBottomSheet(context);
                    },
                    child: const Text('  Edit Meals  '),
                    color: Colors.white,
                ),

                RaisedButton(
                  onPressed: () {
                    _showBottomSheetCallback();
                  },
                  child: const Text('Check Meals'),
                  color: Colors.white,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

///Workout Page
///-----------------------------------------------------------------------------
class Workouts extends StatefulWidget {
  @override
  _WorkoutsState createState() => _WorkoutsState();
}

class _WorkoutsState extends State<Workouts> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String workout = "";

  void _showBottomSheetCallback(String day){
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return Container(
          decoration: BoxDecoration(
              border: Border(top: BorderSide(color: Colors.black)),
              color: Colors.red[100],
          ),
          child: Padding(
            padding: const EdgeInsets.all(32),
            child: Text(determineWorkout(day), textAlign: TextAlign.center, style: TextStyle(fontSize: 24.0),
            ),
          ),
      );
    });
  }

  String determineWorkout(String day) {
    if( day == 'Monday' )
      workout = 'Leg Day                                             ' + "\n" + '4 Sets - 10 Reps Each' + '\n' + 'Squats' + '\n' + 'Lunges' + '\n' + 'Glute Bridges';

    if( day == 'Tuesday' )
      workout = 'Push Day                                             ' + "\n" + '4 Sets - 10 Reps Each' + '\n' + 'Bench' + '\n' + 'Chest Flys' + '\n' + 'Tricep Pushdowns';

    if( day == 'Wednesday' )
      workout = 'Pull Day                                             ' + "\n" + '4 Sets - 10 Reps Each' + '\n' + 'Deadlifts' + '\n' + 'Pull-Ups' + '\n' + 'Bicep Curls';

    if( day == 'Thursday' )
      workout = 'Cardio Day                                             ' + "\n" + '4 Sets - 5 Mins Each' + '\n' + 'Burpees' + '\n' + 'Sprints' + '\n' + 'Jumping Jacks';

    if( day == 'Friday' )
      workout = 'Choose day on lagging muscle group';

    if( day == 'Saturday' || day == 'Sunday' )
      workout = 'Break Day - Go Treat yoself right!';

    return workout;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Workouts'),
        backgroundColor: Colors.red,
      ),
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("images/lungegirl.jpg",),
                fit: BoxFit.fill,
              ),
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  onPressed: () {
                    _showBottomSheetCallback('Monday');
                  },
                  child: const Text('    Monday    '),
                  color: Colors.white,
                ),
                RaisedButton(
                  onPressed: () {
                    _showBottomSheetCallback('Tuesday');
                  },
                  child: const Text('   Tuesday   '),
                  color: Colors.white,
                ),
                RaisedButton(
                  onPressed: () {
                    _showBottomSheetCallback('Wednesday');
                  },
                  child: const Text('Wednesday'),
                  color: Colors.white,
                ),
                RaisedButton(
                  onPressed: () {
                    _showBottomSheetCallback('Thursday');
                  },
                  child: const Text('  Thursday  '),
                  color: Colors.white,
                ),
                RaisedButton(
                  onPressed: () {
                    _showBottomSheetCallback('Friday');
                  },
                  child: const Text('     Friday     '),
                  color: Colors.white,
                ),
                RaisedButton(
                  onPressed: () {
                    _showBottomSheetCallback('Saturday');
                  },
                  child: const Text('   Saturday   '),
                  color: Colors.white,
                ),
                RaisedButton(
                  onPressed: () {
                    _showBottomSheetCallback('Sunday');
                  },
                  child: const Text('    Sunday    '),
                  color: Colors.white,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}